<?php

return [
    'userAgent' => 'https://meteo.comptoir.net',

    /**
     * Données SYNOP essentielles OMM
     * https://donneespubliques.meteofrance.fr/?fond=produit&id_produit=90&id_rubrique=32
     */
    'url' => 'https://donneespubliques.meteofrance.fr/donnees_libres/Txt/Synop',
    'history' => [
        // https://donneespubliques.meteofrance.fr/donnees_libres/Txt/Synop/Archive/synop.199701.csv.gz
        'url' => '/Archive/synop.{year}{month}.csv.gz',
    ],
    'update' => [
        // https://donneespubliques.meteofrance.fr/donnees_libres/Txt/Synop/synop.2022081400.csv
        'url' => '/synop.{year}{month}{day}{time}.csv',
        // Les données ne sont disponilbes que pour certaines heures,
        // la méthode `MeteoFrance::getUpdateUrl()` utilise ce tableau pour définir l'url qui va bien.
        // La liste des heures est au chapitre "téléchargement" de la page des données.
        'times' => [0, 3, 6, 9, 12, 15, 18, 21],
    ],

    // Nombre de jours pour affectation d'une class en fonction de la fraicheur de la dernière mesure (gris, orange, rouge).
    // Voir le template `resources/views/_data-resume.php`.
    'lates' => [1,2,90]
];
