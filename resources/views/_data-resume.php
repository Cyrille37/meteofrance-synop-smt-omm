<?php

use App\Models\Station;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

$data = Cache::rememberForever( Station::CACHEKEY_DATARESUME, function () {
	//Log::debug('Compute cache for Station::CACHEKEY_DATARESUME');
	return DB::select('select S.*, LM.measured_at_min, LM.measured_at_max, missingdata, missingdata_at from stations S
		join (
			select station_id, min(measured_at) as measured_at_min, max(measured_at) as measured_at_max from measures
			group by station_id
		) as LM on LM.station_id = S.id
   		order by S.name');
});

?>

<p>Nombre de stations: <span class="badge"><?= count($data) ?></span></p>
<p>[<b>Nom de la station</b>] [date 1<sup>ère</sup> mesure] ⇒ [date dernière mesure] ([données manquantes sur la période])</p>

<div>
<?php

$now = new Carbon();
$lates = config('meteo.lates');

foreach( $data as $datum )
{
	$daysDiff = $now->diffInDays( $datum->measured_at_max );
	$lateClass = $daysDiff <= $lates[0] ? 'late-0'
			: ($daysDiff <= $lates[1] ? 'late-1'
			: ($daysDiff <= $lates[2] ? 'late-2'
			: 'late-3'))
			;

	echo '<span class="badge">',$datum->name,
		' <span>',$datum->measured_at_min, '</span> ⇒',' <span class="'.$lateClass.'">',$datum->measured_at_max,'</span>',
		' <span class="missing">(', number_format($datum->missingdata,0,',',' ' ),')</span>',
		'</span>',"\n" ;
}

?>
</div>

<div id="map" class="mt-4"></div>

<script>
	var map = L.map('map').setView([20, 0], 2);
	map.scrollWheelZoom.disable();
	L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
		maxZoom: 19,
		attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
	}).addTo(map);

<?php
	foreach( $data as $datum )
	{
		echo 'L.circle(['.$datum->latitude.', '.$datum->longitude.'], { color:"#BE5EB5",fillColor:"#BE5EB5",fillOpacity: 1, radius: 10})
			.bindPopup("'.$datum->name.'")
			.addTo(map);';
	}
?>

</script>
