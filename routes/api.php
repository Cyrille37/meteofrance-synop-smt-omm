<?php

use App\Models\Station;
use App\Http\Controllers\ApiController;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Route;
use Symfony\Component\HttpFoundation\Response as HttpResponse;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
*/

/*Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route
    // max-age=N indique que la réponse reste fraîche jusqu'à N secondes après la génération de la réponse.
    // 900 = 15 minutes
    ::middleware('cache.headers:public;max_age=900;etag')
    ->get('/stations/{omm_code?}', [ApiController::class, 'getStations']);

Route
    // client cache middleware doesnot work here because of streamed response,
    // so cache header are set in response @see ApiController::getSynops().
    //::middleware('cache.headers:public;max_age=900;etag')
    ::get('/synops/{omm_code}/{from?}/{to?}', [ApiController::class, 'getSynops']);

/**
 * @param string $key The cache key, without prefix. Is mandatory to avoid clearing other app' cache.
 * @see \App\Http\Middleware\VerifyCsrfToken::$except
 */
Route::post('/cache/clear/{key?}', function (Request $request, $key = null) {

    Log::debug(__METHOD__, ['uri' => $request->getUri(), 'key' => $key]);

    $cacheAccessKey = Config::get('app.cache_access_key');
    if (!$cacheAccessKey)
        throw new RuntimeException('Missing "cache access key" configuration key.');

    if ($request->header('X-Key') != $cacheAccessKey)
        throw new UnauthorizedHttpException('Invalid Key');

    if (! empty($key) )
    {
        Cache::forget($key);
    }
    else
    {
        Cache::flush();
    }

    return 'Ok';
})->name('cache.clear');
