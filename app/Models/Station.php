<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\QueryException;

/**
 * @property int $id
 * @property string $name
 * @property int $omm_code
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * 
 * 
 */
class Station extends Model
{
    const TABLENAME = 'stations' ;
    protected $table = self::TABLENAME;

    /**
     * Already prefixed via config.cache.prefix. @see /config/cache.php.
     */
    const CACHEKEY_DATARESUME = 'home-data-resume' ;

    // use HasFactory;
    public function measures()
    {
        return $this->hasMany(Measure::class);
    }

    /**
     * Import GZ encoded string formatted as found in documentation on page 
     * https://donneespubliques.meteofrance.fr/?fond=produit&id_produit=90&id_rubrique=32
     * 
     * Lines ending with the csv separator, so filtering the last field.
     */
    public static function importMeteofranceSynopFormat(&$content)
    {
        $lineSeparator = "\r\n";
        $fieldSeparator = ';';

        $headers = null;
        $headersCount = null;
        $linesCount = 0;

        $line = strtok($content, $lineSeparator);
        while ($line !== false) {
            $linesCount++;
            if ($headers == null) {
                $headers = str_getcsv($line, $fieldSeparator);
                array_pop($headers);
                $headersCount = count($headers);
                //echo 'headers: '.var_export($headers,true),"\n";
            } else {
                $data = str_getcsv($line, $fieldSeparator);

                if ((count($data) - 1) != $headersCount)
                    throw new \InvalidArgumentException('data count ' . count($data) . ' != headers count ' . $headersCount . '. linesCount=' . $linesCount);

                // Measure values
                $measureArray = [];
                foreach ($headers as $i => $h) {
                    if ($i == $headersCount)
                        continue;
                    // unknow value is marked with 'mq' and processed in the following "switch.default".
                    if ($data[$i] == '')
                        throw new \InvalidArgumentException('empty field linesCount=' . $linesCount . ', i=' . $i . ', h:' . $h . ', d:' . $data[$i]);

                    // Process each data field
                    switch ($h) {
                        case 'date':
                            //echo 'measured_at:', $data[$i],"\n";
                            $measureArray['measured_at'] = new Carbon($data[$i]);
                            break;
                        case 'numer_sta':
                            //echo 'numer_sta:', $data[$i],"\n";
                            $station = Station::where('omm_code', $data[$i])->firstOrFail();
                            $measureArray['station_id'] = $station->id;
                            break;
                        default:
                            if ($data[$i] == 'mq')
                                $measureArray[$h] = null;
                            else
                                $measureArray[$h] = $data[$i];
                    }
                }
                //echo 'measureArray: ', var_export($measureArray,true),"\n";

                try {
                    Measure::updateOrCreate(
                        ['station_id'=>$measureArray['station_id'], 'measured_at'=>$measureArray['measured_at']],
                        $measureArray
                    );
                } catch (QueryException $ex) {
                    switch ($ex->getCode()) {
                        //case 23000:
                        //    break;
                        default:
                            throw $ex;
                    }
                }
            }
            $line = strtok($lineSeparator);
        }

        return $linesCount;
    }
}
