<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $station_id
 * @property Carbon $measured_at
 * 
 */
class Measure extends Model
{
    const TABLENAME = 'measures' ;
    protected $table = self::TABLENAME;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false ;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    public function station()
    {
        return $this->belongsTo(Station::class);
    }
}
