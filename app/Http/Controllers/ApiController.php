<?php

namespace App\Http\Controllers;

use App\Models\Station;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Route;
use Symfony\Component\HttpFoundation\Response as HttpResponse;

class ApiController extends Controller
{
    const CACHEKEY_STATIONS = 'stations';

    const DEFAULT_LIMIT = 365 * 4 ;

    /**
     * All stations
     * curl --insecure -i -H "Accept: application/json" http://127.0.0.1:8000/api/stations
     * The 89642 station
     * curl --insecure -i -H "Accept: application/json" http://127.0.0.1:8000/api/stations/89642
     *
     * Clear the cache:
     * curl --insecure -I -X POST -H 'X-Key: ABC123' https://meteo.devhost/api/cache/clear
     *
     */
    public function getStations($omm_code = null)
    {
        $data = Cache::rememberForever(
            self::CACHEKEY_STATIONS . ($omm_code ? '-' . $omm_code : ''),
            function () use ($omm_code) {

                Log::debug(__CLASS__.' getStations() recompute data');

                $q = Station::query();
                if ($omm_code)
                    $q->where('omm_code', $omm_code);

                return $q->get();
            }
        );

        return Response::json(
            ['stations' => $data,],
            HttpResponse::HTTP_OK,
            [],
            JSON_HEX_AMP | JSON_NUMERIC_CHECK | JSON_PRESERVE_ZERO_FRACTION
        );
    }

    /**
     * Other optional parameters can be set on query string:
     * - last : if present, result contains only the last (freshest) data.
     * - fields : si présent, ne répond que les champs demandés
     *
     * curl --insecure -i -H "Accept: application/json" https://meteo.devhost/api/synops/89642
     *
     * @param Request $request
     * @param string $omm_code
     * @param string $from_at Date formatted for Carbon
     * @param string $to_at Date formatted for Carbon
     */
    public function getSynops(Request $request, $omm_code, $from_at = null, $to_at = null)
    {
        //throw new \InvalidArgumentException('invalid arg');
        $station = Station::where('omm_code', $omm_code)->firstOrFail();

        $q = $station->measures();

        // optional "fields"
        if ($request->has('fields')) {
            $fields = explode(',', $request->get('fields'));
            if (!in_array('measured_at', $fields))
                $fields[] = 'measured_at';
            $q->select($fields);
        }

        if ($from_at)
        {
            $d = new Carbon($from_at) ;
            if( strlen($from_at) == 10 )
                $d->setTime(0,0,0,0);
            //Log::debug(__METHOD__,['$from_at'=>$d]);
            $q->where('measured_at', '>=', $d);
        }

        if ($to_at)
        {
            $d = new Carbon($to_at) ;
            if( strlen($to_at) == 10 )
            {
                $d->setTime(23,59,59,999*1000);
            }
            //Log::debug(__METHOD__,['$to_at'=>$d]);
            $q->where('measured_at', '<=', new Carbon($d));
        }

        // optional "last"
        if ($request->has('last')) {

            //$q->orderBy('measured_at', 'desc');
            $q->latest('measured_at');
            $q->limit(1);

        } else {

            $q->orderBy('measured_at', 'asc');

            if( !$to_at && !$from_at )
                $q->limit( self::DEFAULT_LIMIT );
        }

        /**
         * JSON response is streamed to permits big query result without runout off allowed memory.
         * The query use cursor.
         * Use json_encode() on item instead of the collection.
         */
        return Response::stream(
            function () use ($station, $q) {
                echo '{ "station":', json_encode($station, JSON_HEX_AMP | JSON_NUMERIC_CHECK), ',';
                echo '"measures":[';
                $first = true;
                foreach ($q->cursor() as $measure) {
                    if ($first) {
                        $first = false;
                    } else {
                        echo ',';
                    }
                    echo json_encode($measure, JSON_NUMERIC_CHECK | JSON_PRESERVE_ZERO_FRACTION);
                    //flush();
                }
                echo ']}';
            },
            HttpResponse::HTTP_OK,
            [
                'Content-Type' => 'application/json',
                // Nginx specific: disable fastcgi buffering
                // http://nginx.org/en/docs/http/ngx_http_fastcgi_module.html#fastcgi_buffering
                'X-Accel-Buffering' => 'no',
                'Cache-Control' => 'max-age=900, public',
            ]
        );
    }
}
