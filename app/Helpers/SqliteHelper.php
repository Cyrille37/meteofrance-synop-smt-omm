<?php

namespace App\Helpers;

use Illuminate\Database\ConnectionInterface;
use Illuminate\Database\DatabaseManager;
use Illuminate\Database\Query\Expression;
use Illuminate\Database\SQLiteConnection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SqliteHelper
{
    /**
     * Laravel: Optimizing SQLite to dangerous speeds
     * https://kerkour.com/sqlite-for-servers
     * https://medium.com/swlh/laravel-optimizing-sqlite-to-dangerous-speeds-ff04111b1f22
     */
    public function initialize(DatabaseManager $manager)
    {
        try {
            $db = $manager->connection();
            if ($db instanceof SQLiteConnection) {
                $this->setSqliteJournalMode($db);
                $db->statement(
                    'PRAGMA synchronous = OFF;'
                );
            }
        } catch (\Throwable $throwable) {
            Log::error(__METHOD__, ['throwable'=>$throwable]);
            return;
        }
    }

    /**
     * Set Sqlite Journal Mode, default to "WAL"
     *
     * @param  \Illuminate\Database\ConnectionInterface $connection
     * @return string
     */
    protected function setSqliteJournalMode(ConnectionInterface $db, $mode = 'WAL')
    {
        $journal = data_get($db->select(new Expression('PRAGMA journal_mode')), '0.journal_mode');
        if ($journal != 'wal') {
            Log::info('Settings SQLite journal mode to "wal"');
            $db->statement(
                'PRAGMA journal_mode=WAL;'
            );
        }
    }
}