<?php

namespace App\Helpers;

use App\Models\Station;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use RuntimeException;

class DataHelper
{
    public function computeMissingData()
    {
        $dataMissing = DB::select('
        with dates as (SELECT DISTINCT measured_at FROM measures )
        select r.id as id, count(*) as missingdata
            from (
                SELECT s.id as id, s.name AS nom_station, dates.measured_at
                FROM stations s
                CROSS JOIN dates
                LEFT JOIN measures m ON s.id = m.station_id AND dates.measured_at = m.measured_at
                WHERE m.station_id IS NULL
                    and dates.measured_at >= (select min(measured_at) from measures mm where mm.station_id=s.id)
                    and dates.measured_at <= (select max(measured_at) from measures mm where mm.station_id=s.id)
            ) r
            group by r.id
        ');

        $now = new Carbon();
        foreach( $dataMissing as $datum )
        {
            Station::where('id', $datum->id)
              ->update(['missingdata' => $datum->missingdata, 'missingdata_at'=>$now]);
        }

    }
}
