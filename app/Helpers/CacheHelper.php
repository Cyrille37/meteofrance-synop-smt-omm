<?php

namespace App\Helpers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Config;
use RuntimeException;

class CacheHelper
{
    /**
     * APCu could be disabed for CLI, so we call a dedicated endpoint to clear a cache key.
     *
     * `curl --insecure -X POST -H 'X-Key: ABC123' https://meteo.devhost/cache/clear/home-data-resume`
     */
    public function cacheClear($key = null)
    {
        // change: clear all
        //$url = \route('cache.clear', ['key'=>Station::CACHEKEY_DATARESUME]);
        //$url = \route('cache.clear');
        $url = \route('cache.clear', ['key' => $key]);

        try {
            $response = Http
                ::withOptions([
                    'verify' => App::environment('production'),
                ])
                ->withHeaders([
                    'X-Key' => Config::get('app.cache_access_key'),
                ])
                ->post($url);

            if (!$response->successful())
                throw new RuntimeException('failed with HTTP ' . $response->status());

        } catch (\Exception $ex) {
            throw new RuntimeException( __METHOD__ .' failed with HTTP ' . $ex->getMessage() );
        }
    }
}
