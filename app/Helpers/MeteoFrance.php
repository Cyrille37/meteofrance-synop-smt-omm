<?php

namespace App\Helpers;

use Carbon\Carbon;
use Illuminate\Support\Facades\Config;

class MeteoFrance {

    /**
     * Compute the update url for a date $forDate.
     * @param Carbon $forDate
     */
    public static function getUpdateUrl( Carbon $forDate )
    {
        $urlPattern = Config::get('meteo.url') . Config::get('meteo.update.url');
        $times = Config::get('meteo.update.times');

        if( ! in_array($forDate->hour, $times) )
        {
            return null ;
        }

        return str_replace(
            ['{year}', '{month}', '{day}', '{time}'],
            [
                $forDate->year,
                str_pad($forDate->month, 2, '0', STR_PAD_LEFT),
                str_pad($forDate->day, 2, '0', STR_PAD_LEFT),
                str_pad($forDate->hour, 2, '0', STR_PAD_LEFT)
            ],
            $urlPattern
        );
    }

    public static function getArchiveUrl( Carbon $forDate )
    {
        $urlPattern = Config::get('meteo.url') . Config::get('meteo.history.url');

        return str_replace(
            ['{year}', '{month}'],
            [$forDate->year, str_pad($forDate->month, 2, '0', STR_PAD_LEFT)],
            $urlPattern
        );

    }
}