<?php

namespace App\Console\Commands;

use App\Helpers\CacheHelper;
use App\Helpers\DataHelper;
use App\Helpers\MeteoFrance;
use App\Models\Station;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Http\Client\HttpClientException;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Symfony\Component\Console\Output\OutputInterface;

class Update extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'meteo:update
        {--cacheFolder= : The folder where to store downloaded files, no cache if NULL.}
        {--dateTime= : to force the update date time.}
        ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Download meteo data update';

    const CACHE_FILE_PREFIX = 'meteofrance_';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $cacheFolder = $this->option('cacheFolder');
        if ($cacheFolder && (!is_dir($cacheFolder) || !is_writable($cacheFolder)))
            throw new \InvalidArgumentException('cacheFolder must exists and writable.');
        $cacheFile = null;

        $now = $this->option('dateTime');
        if( empty($now) )
        {
            // Find the matching time in "times" settings
            $now = (new Carbon())->timezone('Etc/UTC');
        }
        else
        {
            $now = (new Carbon($now))->timezone('Etc/UTC');            
        }

        $url = MeteoFrance::getUpdateUrl($now);

        if (empty($url)) {
            $this->comment('Not time for data: ' . $now->toDateTimeString(), OutputInterface::VERBOSITY_VERBOSE);
            return;
        }

        $content = null;

        if ($cacheFolder) {
            preg_match('#.*/synop.(\d+).csv$#', $url, $m);
            $cacheFile = $cacheFolder . DIRECTORY_SEPARATOR . self::CACHE_FILE_PREFIX . 'synop.' . $m[1] . ' Y' . $now->year . 'M' . $now->month . 'T' . $now->hour . '.csv';
            if (file_exists($cacheFile)) {
                $this->info('from cache at ' . $now->toDateTimeString(), OutputInterface::VERBOSITY_VERBOSE);
                $content = file_get_contents($cacheFile);
            }
        }

        if (!$content) {

            $this->info('downloading at ' . $now->toDateTimeString(), OutputInterface::VERBOSITY_VERBOSE);
            $this->comment('url: ' . $url, OutputInterface::VERBOSITY_VERBOSE);
            Log::info(__METHOD__, ['url' => $url]);

            /**
             * https://docs.guzzlephp.org
             */
            $http = new \GuzzleHttp\Client(['headers' => [
                'User-Agent' => Config::get('meteo.userAgent')
            ]]);

            $res = $http->request('GET', $url, []);
            //$this->comment('http status: '.$res->getStatusCode().', http content-type: '.$res->getHeader('content-type')[0] );
            if ($res->getHeader('content-type')[0] != 'text/plain; charset=ISO-8859-1')
                throw new HttpClientException('File not found at ' . $url, 404);

            $content = $res->getBody();

            if ($cacheFile)
                file_put_contents($cacheFile, $content);
        }

        $linesCount = Station::importMeteofranceSynopFormat(
            $content
        );

        $this->comment('Processed ' . $linesCount . ' lines', OutputInterface::VERBOSITY_VERBOSE);

        App::call([new DataHelper, 'computeMissingData']);

        //$this->cacheClear();
        App::call([new CacheHelper, 'cacheClear']);

    }

}
