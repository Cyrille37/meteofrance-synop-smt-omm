<?php

namespace App\Console\Commands;

use App\Helpers\MeteoFrance;
use App\Models\Measure;
use App\Models\Station;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Http\Client\HttpClientException;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;

class CheckStationsList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'meteo:checkStationsList';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check if stations list is uptodate.';

    const STATIONS_LIST_URL = 'https://donneespubliques.meteofrance.fr/donnees_libres/Txt/Synop/postesSynop.json';
    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        /**
         * https://docs.guzzlephp.org
         */
        $http = new \GuzzleHttp\Client(['headers' => [
            'User-Agent' => Config::get('meteo.userAgent')
        ]]);

        $res = $http->request('GET', self::STATIONS_LIST_URL, []);

        // Il n'y a pas de 404 mais un 200 avec une page qui affiche "Fichier non trouvé" :-(
        //$this->comment('http status: '.$res->getStatusCode().', http content-type: '.$res->getHeader('content-type')[0] );
        if ($res->getHeader('content-type')[0] != 'text/plain; charset=ISO-8859-1')
            throw new HttpClientException($res->getReasonPhrase(), 404);

        $stationsJson = json_decode($res->getBody());
        if (!$stationsJson)
            throw new \RuntimeException('Failed to decode Json data');

        foreach ($stationsJson->features as $feature) {
            // "ID": "07005", "Nom": "ABBEVILLE", "Latitude": "50.136000", "Longitude": "1.834000"
            $station = Station::where('omm_code', $feature->properties->ID)->first();
            if (!$station) {
                $this->warn('New station: ' . $feature->properties->Nom);
                continue;
            }
            if ($station->name != $feature->properties->Nom)
                $this->warn('Station ' . $station->name . ' name changed');
            if ($station->latitude != $feature->properties->Latitude)
                $this->warn('Station ' . $station->name . ' latitude changed');
            if ($station->longitude != $feature->properties->Longitude)
                $this->warn('Station ' . $station->name . ' longitude changed');
        }
    }
}
