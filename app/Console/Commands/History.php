<?php

namespace App\Console\Commands;

use App\Helpers\CacheHelper;
use App\Helpers\DataHelper;
use App\Helpers\MeteoFrance;
use App\Models\Measure;
use App\Models\Station;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Http\Client\HttpClientException;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class History extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'meteo:history'
        . ' {startDate : Mandatory start date for history update.}'
        . ' {--endDate= : Optional end date for history update.}'
        . ' {--sleep=20 : Sleep secondes between http query to avoid spamming the server.}'
        . ' {--cacheFolder= : The folder where to store downloaded files, no cache if NULL.}'
        . ' {--dontUpdateDb : Only download, do not update database.}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Download meteo data history';

    const CACHE_FILE_PREFIX = 'meteofrance_';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $sleepSeconds = $this->option('sleep');
        $cacheFolder = $this->option('cacheFolder');
        $dontUpdateDb = $this->option('dontUpdateDb');
        $startDate = $this->argument('startDate');
        $endDate = new Carbon($this->option('endDate'));

        if ($cacheFolder && (!is_dir($cacheFolder) || !is_writable($cacheFolder)))
            throw new \InvalidArgumentException('cacheFolder must exists and writable.');

        if (!$startDate)
            throw new \InvalidArgumentException('Invalid startDate');

        $startDate = new Carbon($startDate);

        $this->info('startDate: ' . $startDate->toDateTimeString());
        $this->info('endDate: ' . $endDate->toDateTimeString());
        $this->info('cacheFolder: ' . var_export($cacheFolder, true));
        $this->info('dontUpdateDb: ' . var_export($dontUpdateDb, true));
        $this->info('sleep: ' . $sleepSeconds . ' seconds');

        $stationsCountOriginal = $this->getMeasuresCountByStations();

        /**
         * https://docs.guzzlephp.org
         */
        $http = new \GuzzleHttp\Client(['headers' => [
            'User-Agent' => Config::get('meteo.userAgent')
        ]]);

        $loopDate = $startDate;
        $mustSleep = false;
        while ($loopDate < $endDate) {
            $content = null;
            $cacheFile = null;

            if ($cacheFolder) {
                $cacheFile = $cacheFolder . DIRECTORY_SEPARATOR . self::CACHE_FILE_PREFIX . $loopDate->year . $loopDate->month . '.csv.gz';
                if (file_exists($cacheFile)) {
                    $content = file_get_contents($cacheFile);
                }
            }

            if (!$content) {

                if ($mustSleep) {
                    $this->comment('Sleeping ' . $sleepSeconds . ' secondes...');
                    sleep($sleepSeconds);
                }
                $mustSleep = true;

                $url = MeteoFrance::getArchiveUrl($loopDate);
                $this->comment('downloading Y:' . $loopDate->year . ' M:' . $loopDate->month . ' at "' . $url . '"');
                $res = $http->request('GET', $url, []);
                /* Quand fichier non trouvé, le http status est 200 :-(
                $this->comment('http status: '.$res->getStatusCode().', http content-type: '.$res->getHeader('content-type')[0] );
                Not found: http status: 200, http content-type: text/html; charset=utf-8
                Found: http status: 200, http content-type: application/x-gzip
                */
                if ($res->getHeader('content-type')[0] != 'application/x-gzip')
                    throw new HttpClientException($res->getReasonPhrase(), 404);
                $content = $res->getBody();
                if ($cacheFile)
                    file_put_contents($cacheFile, $content);
            } else {
                $this->comment('use cached Y:' . $loopDate->year . ' M:' . $loopDate->month . ' from "' . $cacheFile . '"');
            }

            DB::beginTransaction();

            $stationsCountPass = $this->getMeasuresCountByStations();

            $content = gzdecode($content);
            $linesCount = Station::importMeteofranceSynopFormat(
                $content
            );
            $this->comment('Processed ' . $linesCount . ' lines');

            $stationsCountDiff = $stationsCountPass->diffAssoc($this->getMeasuresCountByStations());
            if( $stationsCountDiff->count() )
                $this->info( 'Stations mises à jour: '.$stationsCountDiff->count() );

            if ($dontUpdateDb) {
                $this->warn('DB not updated ;-)');
                DB::rollBack();
            } else {
                DB::commit();
            }

            $loopDate->addMonth(1);
        }

        $stationsCountDiff = $stationsCountOriginal->diffAssoc($this->getMeasuresCountByStations());
        $this->info( 'Stations total mises à jour: '.$stationsCountDiff->count() );

        App::call([new DataHelper, 'computeMissingData']);

        App::call([new CacheHelper, 'cacheClear']);

        return 0;
    }

    protected function getMeasuresCountByStations()
    {
        return Station::withCount('measures')->get()->mapWithKeys(function (Station $item, int $key) {
            return [$item->omm_code => $item->measures_count];
        });
    }
}
