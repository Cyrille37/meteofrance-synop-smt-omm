<?php
 
namespace App\Logging;
 
use Monolog\Formatter\LineFormatter;
 
class CustomizeLog
{
    /**
     * Customize log filename to separate web request and cli command.
     *
     * @param  \Illuminate\Log\Logger  $logger
     * @return void
     */
    public function __invoke($logger)
    {
        $processUser = posix_getpwuid( posix_geteuid() );
        $processName = $processUser[ 'name' ];
        $filename = storage_path( 'logs/laravel-' . php_sapi_name() . '-' . $processName . '.log' );
        $handler = new \Monolog\Handler\RotatingFileHandler( $filename );

        $logger->setHandlers( [$handler] );

    }
}