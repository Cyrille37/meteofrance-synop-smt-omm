<?php

namespace App\Providers;

use App\Helpers\SqliteHelper;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\App;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Optimizing SQLite for server
        App::call([new SqliteHelper, 'initialize']);
    }

}
