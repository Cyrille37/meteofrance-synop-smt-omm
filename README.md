Ce code implémente une API d'accès aux données MeteoFrance "[Données SYNOP essentielles OMM](https://donneespubliques.meteofrance.fr/?fond=produit&id_produit=90&id_rubrique=32)".

Leur définition est :
- Données d'observations issues des messages internationaux d’observation en surface (SYNOP) circulant sur le système mondial de télécommunication (SMT) de l’Organisation Météorologique Mondiale (OMM). Paramètres atmosphériques mesurés (température, humidité, direction et force du vent, pression atmosphérique, hauteur de précipitations) ou observés (temps sensible, description des nuages, visibilité) depuis la surface terrestre. Selon instrumentation et spécificités locales, d'autres paramètres peuvent être disponibles (hauteur de neige, état du sol, etc.)
- Métropole et outre-mer - Fréquence : 3 h
- Depuis janvier 1996.

- OMM Organisation Mondiale de la Météorologie
  - WMO  World Meteorological Organization
  - [Manual on Codes, 2019](https://library.wmo.int/doc_num.php?explnum_id=10235)

Ce code est partagé selon le contrat de la licence [GNU GENERAL PUBLIC LICENSE V3](LICENSE).

## Service

Une instance est en libre accès sur https://meteo.comptoir.net/

## Installation

Récupérer le projet:
```bash
git clone https://framagit.org/Cyrille37/meteofrance-synop-smt-omm.git meteofrance-synop
cd meteofrance-synop
composer install
```

Configure a database and update `.env`.

Create table and insert referencial data.
```bash
./artisan migrate
./artisan db:seed --class=StationSeeder
```

Mise à jour des données depuis "SYNOP essentielles OMM" de Météo France:
```bash
./artisan meteo:update
```
À exécuter toutes les heures, le download effectif ne sera réalisé qu'aux heures des fichiers synop (voir config('meteo.update.times')).

## Api

### Stations

All stations:
`curl -i -H 'Accept: application/json' 'http://127.0.0.1:8000/api/stations'`

Only station 89642 (*OMM ID*):
`curl -i -H 'Accept: application/json' 'http://127.0.0.1:8000/api/stations/89642'`

### Measures

All measures for station 89642:
`curl -i -H 'Accept: application/json' 'http://127.0.0.1:8000/api/synops/89642'`

Last measure for station 89642:
`curl -i -H 'Accept: application/json' 'http://127.0.0.1:8000/api/synops/89642?last'`

All measures from 2022-08-13 for station 89642:
`curl -i -H 'Accept: application/json' 'http://127.0.0.1:8000/api/synops/89642/2022-08-13'`

All measures from 2022-08-13 for station 89642 but only fields "measured_at" and "t":
`curl -i -H 'Accept: application/json' 'http://127.0.0.1:8000/api/synops/89642/2022-08-13?fields=measured_at,t'`

Last measures of day 2022-01-01:
`curl -i -H 'Accept: application/json' 'http://127.0.0.1:8000/api/synops/89642/2022-01-01/2022-01-01%2023:59:59?last&fields=measured_at,t'`

Or you can replace "from" with a "0":
`curl -i -H 'Accept: application/json' 'http://127.0.0.1:8000/api/synops/89642/0/2022-01-01%2023:59:59?last&fields=measured_at,t'`

### Cache

Si le driver du cache est APC la commande `./artisan cache:clear` risque de ne pas fonctionner, dans ce cas utiliser l'API:

`curl --insecure -I -X POST -H 'X-Key: ABC123' https://meteo.devhost/api/cache/clear`

La valeur de `X-Key` est celle renseignée à la clé `CACHE_ACCESS_KEY` dans le fichier `.env`.

## About data

Il peut y avoir des doublons "station + date" dans les fichiers.
Exemple: fichier "synop.200208.csv" station "7299" le "20020805120000".

Volumétrie des données du 1996-01-01 au 2022-08-13:
- Fichier .GZ : 308 éléments, totalisant 161,7 Mo. Importées dans SQLite 
- SQLite (ya des indexes!): 656 Mo, 62 stations, 4 310 243 measures.
  - 571 Mo sans l'index ['station_id','measured_at']
Volumétrie au 2023-05-19:
- Fichier Sqlite: .tgz 220 Mo .sqlite 829 Mo

## About design

Icône météo de Rosa Suave: [Weather forecast icon created by Rosa Suave - Flaticon](https://www.flaticon.com/free-icons/weather-forecast).

## Story

### 2023-05-20

- nouvelle commande `meteo:checkStationsList` pour vérifier la liste des stations.

### 2023-05-19

- refacto: déplace fonctions du fichier des routes vers `Controller`.
- cache: plus de cache serveur et client
- icône du projet
- documentation

### 2023-05-10

Jusqu'au 2022-08-18 il y avait 8 mesures par jour, depuis il y en a 7 ou moins, il manque donc des données :-(
```sql
select D, C, stations.omm_code, stations.name from
(
  select substr(measured_at,1,10) as D, station_id as S, count(*) as C from measures
  where measured_at >= "2022-08-01" and measured_at <= "2022-10-31"
  group by substr(measured_at,1,10), station_id
  order by D desc
)
left join stations on stations.id = S
where C <> 8
order by D desc, stations.name asc
```
Pour une reprise de données: `./artisan meteo:history 2022-09-01 --endDate=2023-05-31`

**Certaines données manquent à cause d'erreur de communication, d'autres parce qu'elles n'existent pas ...** Et on dirait que parfois les archives sont mises à jour plus tard ...

### 2022-08

1ères lignes de code pour alimenter les graphiques des [compteurs de vélo de la ville de Tours](https://cyrille37.frama.io/compteurs-velos-a-tours/).
