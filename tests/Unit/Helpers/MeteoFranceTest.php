<?php

namespace Tests\Unit\Helpers;

use App\Helpers\MeteoFrance;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Support\Str;

class MeteoFranceTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetUpdateUrl()
    {
        // At 01 it should be 00
        $d = new Carbon('2022-08-12 01:00:00', 'Etc/UTC');
        $url = MeteoFrance::getUpdateUrl($d);
        $this->assertTrue(
            Str::endsWith($url,'synop.2022081200.csv')
        );

        // At 11 it should be 09
        $d = new Carbon('2022-08-12 11:00:00', 'Etc/UTC');
        $url = MeteoFrance::getUpdateUrl($d);
        $this->assertTrue(
            Str::endsWith($url,'synop.2022081209.csv')
        );

        // At 12 it should be 09 because 12 might not be ready
        $d = new Carbon('2022-08-12 12:00:00', 'Etc/UTC');
        $url = MeteoFrance::getUpdateUrl($d);
        $this->assertTrue(
            Str::endsWith($url,'synop.2022081209.csv')
        );

        // At 00 it must be the day before at 21
        $d = new Carbon('2022-08-12 00:00:00', 'Etc/UTC');
        $url = MeteoFrance::getUpdateUrl($d);
        $this->assertTrue(
            Str::endsWith($url,'synop.2022081121.csv')
        );

        // At first hour of first day of month it must be 21 for last day of previous month
        $d = new Carbon('2022-08-01 00:00:00', 'Etc/UTC');
        $url = MeteoFrance::getUpdateUrl($d);
        $this->assertTrue(
            Str::endsWith($url,'synop.2022073121.csv')
        );
    }
}
