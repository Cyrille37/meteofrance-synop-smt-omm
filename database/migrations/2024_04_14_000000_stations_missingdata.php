<?php

use App\Models\Station;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class StationsMissingData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(Station::TABLENAME, function (Blueprint $table) {
            $table->integer('missingdata')->nullable();
            $table->timestamp('missingdata_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(Station::TABLENAME, function (Blueprint $table) {
            $table->dropColumn(['missingdata', 'missingdata_at']);
        });
    }
}
