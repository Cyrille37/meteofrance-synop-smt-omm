<?php

use App\Models\Station;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Station::TABLENAME, function (Blueprint $table) {
            $table->id();
            $table->string('name', 140)->unique();
            $table->integer('omm_code')->unique();
            $table->double('longitude')->nullable();
            $table->double('latitude')->nullable();
            $table->integer('altitude')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Station::TABLENAME);
    }
}
