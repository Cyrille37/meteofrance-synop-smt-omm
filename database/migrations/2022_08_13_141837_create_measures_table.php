<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMeasuresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('measures', function (Blueprint $table) {

            $table->id();
            //$table->integer('station_id');
            //$table->foreign('station_id')->references('id')->on('stations');
            $table->foreignId('station_id')->constrained('stations');
            $table->datetime('measured_at');
            $table->unique(['station_id','measured_at']);

	
            $table->integer('pmer')->nullable()->comment('pression au niveau mer, Pa');
            $table->integer('tend')->nullable()->comment('variation de pression en 3 heures, Pa');
            $table->integer('cod_tend')->nullable()->comment('type de tendance barométrique, OMM-0200');
            $table->integer('dd')->nullable()->comment('direction du vent moyen 10mn, degré');
            $table->double('ff')->nullable()->comment('vitesse du vent moyen 10mn, m/s');
            $table->double('t')->nullable()->comment('température, K');
            $table->double('td')->nullable()->comment('point de rosée, K');
            $table->integer('u')->nullable()->comment('humidité, %');
            $table->double('vv')->nullable()->comment('visibilité horizontale, mètre');
            $table->integer('ww')->nullable()->comment('temps présent, OMM-4677');
            $table->integer('w1')->nullable()->comment('temps passé 1, OMM-4561');
            $table->integer('w2')->nullable()->comment('temps passé 2, OMM-4561');
            $table->double('n')->nullable()->comment('nébulosité totale, %');
            $table->integer('nbas')->nullable()->comment('nébulosité des nuages de l’étage inférieur, octa');
            $table->integer('hbas')->nullable()->comment('hauteur de la base des nuages de l’étage inférieur, mètre');
            $table->integer('cl')->nullable()->comment('type des nuages de l’étage inférieur, OMM-0513');
            $table->integer('cm')->nullable()->comment('type des nuages de l’étage moyen, OMM-0515');
            $table->integer('ch')->nullable()->comment('type des nuages de l’étage supérieur, OMM-0509');
            $table->integer('pres')->nullable()->comment('pression station, Pa');
            $table->integer('niv_bar')->nullable()->comment('niveau barométrique, Pa');
            $table->integer('geop')->nullable()->comment('géopotentiel, m2/s2');
            $table->integer('tend24')->nullable()->comment('variation de pression en 24 heures, Pa');
            $table->double('tn12')->nullable()->comment('température minimale sur N heures, K');
            $table->double('tn24')->nullable()->comment('température minimale sur N heures, K');
            $table->double('tx12')->nullable()->comment('température maximale sur N heures, K');
            $table->double('tx24')->nullable()->comment('température maximale sur N heures, K');
            $table->double('tminsol')->nullable()->comment('température minimale du sol sur 12 heures, K');
            $table->integer('sw')->nullable()->comment('méthode mesure tw, OMM-3855');
            $table->double('tw')->nullable()->comment('température du thermomètre mouillé, K');
            $table->double('raf10')->nullable()->comment('rafales sur les 10 dernières minutes, m/s');
            $table->double('rafper')->nullable()->comment('rafales sur une période, m/s');
            $table->double('per')->nullable()->comment('période de mesure de la rafale, minute');
            $table->integer('etat_sol')->nullable()->comment('état du sol, OMM-0901');
            $table->double('ht_neige')->nullable()->comment('hauteur totale de la couche de neige, glace, autre, au sol, mètre');
            $table->double('ssfrais')->nullable()->comment('hauteur de la neige fraîche, mètre');
            $table->double('perssfrai')->nullable()->comment('Période de mesure de la neige fraîche, 1/10 heure');
            $table->double('rr1')->nullable()->comment('Précipitations dans les 1 dernières heures, mm');
            $table->double('rr3')->nullable()->comment('Précipitations dans les 3 dernières heures, mm');
            $table->double('rr6')->nullable()->comment('Précipitations dans les 6 dernières heures, mm');
            $table->double('rr12')->nullable()->comment('Précipitations dans les 12 dernières heures, mm');
            $table->double('rr24')->nullable()->comment('Précipitations dans les 24 dernières heures, mm');
            $table->double('phenspe1')->nullable()->comment('Phénomène spécial, OMM-3778');
            $table->double('phenspe2')->nullable()->comment('Phénomène spécial, OMM-3778');
            $table->double('phenspe3')->nullable()->comment('Phénomène spécial, OMM-3778');
            $table->double('phenspe4')->nullable()->comment('Phénomène spécial, OMM-3778');
            $table->integer('nnuage1')->nullable()->comment('Nébulosité couche nuageuse 1, octa');
            $table->integer('nnuage2')->nullable()->comment('Nébulosité couche nuageuse 2, octa');
            $table->integer('nnuage3')->nullable()->comment('Nébulosité couche nuageuse 3, octa');
            $table->integer('nnuage4')->nullable()->comment('Nébulosité couche nuageuse 4, octa');
            $table->integer('ctype1')->nullable()->comment('Type de nuage 1, 0MM-0500');
            $table->integer('ctype2')->nullable()->comment('Type de nuage 2, 0MM-0500');
            $table->integer('ctype3')->nullable()->comment('Type de nuage 3, 0MM-0500');
            $table->integer('ctype4')->nullable()->comment('Type de nuage 4, 0MM-0500');
            $table->integer('hnuage1')->nullable()->comment('Hauteur de base de nuage 1, mètre');
            $table->integer('hnuage2')->nullable()->comment('Hauteur de base de nuage 2, mètre');
            $table->integer('hnuage3')->nullable()->comment('Hauteur de base de nuage 3, mètre');
            $table->integer('hnuage4')->nullable()->comment('Hauteur de base de nuage 4, mètre');
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('measures');
    }
}
