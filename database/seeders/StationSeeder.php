<?php

namespace Database\Seeders;

use App\Models\Station;
use Illuminate\Database\Seeder;

class StationSeeder extends Seeder
{
    /**
     * Seed "stations" table from file "stations.txt" (in this folder).
     * 
     * Le fichier `postesSynop.json` est disponible là:
     * https://donneespubliques.meteofrance.fr/donnees_libres/Txt/Synop/postesSynop.json
     *
     * @return void
     */
    public function run()
    {
        $stations = [];
        $data = json_decode( file_get_contents(__DIR__.'/postesSynop.json') );
        foreach( $data->features as $i => $feature )
        {
            $stations[] = [
                'omm_code' => $feature->properties->ID,
                'name' => $feature->properties->Nom,
                'longitude' => $feature->properties->Longitude,
                'latitude' => $feature->properties->Latitude,
                'altitude' => $feature->properties->Altitude,
            ];
        }
        // https://laravel.com/docs/8.x/queries#upserts
        Station::upsert( $stations,
            ['omm_code'],
            ['longitude','latitude','altitude']
        );
    }

}
